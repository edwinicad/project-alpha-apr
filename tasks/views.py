from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:
        form = TaskForm()

    context = {"form": form}
    return render(request, "tasks/task_form.html", context)


@login_required
def my_tasks(request):
    tasks_assigned = Task.objects.filter(assignee=request.user)

    context = {"tasks": tasks_assigned}
    return render(request, "tasks/my_tasks.html", context)
